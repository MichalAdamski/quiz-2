package com.quiz.classifier;

import com.quiz.patient.Patient;

public interface IDiabetesClassifier {
    void classify(Patient patient);
}
