package com.quiz.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class TextFileLoader {
    public static List<String> loadText(String path) {
        try {
            var list = new ArrayList<String>();
            var file = new File(path);

            if(!file.exists()){
                return null;
            }

            var bufferedReader = new BufferedReader(new FileReader(file));
            var line = "";

            while ((line = bufferedReader.readLine()) != null) {
                list.add(line);
            }

            bufferedReader.close();
            return list;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
