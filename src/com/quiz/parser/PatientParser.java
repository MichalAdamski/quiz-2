package com.quiz.parser;

import com.quiz.patient.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientParser implements IParser<Patient> {
    @Override
    public Patient parse(String line) {
        var patientData = line.split("\\|");
        var patient = new Patient(
                patientData[0],
                Integer.parseInt(patientData[1]),
                patientData[2],
                Integer.parseInt(patientData[3]),
                Double.parseDouble(patientData[4])
        );

        return patient;
    }

    @Override
    public List<Patient> parse(List<String> records) {
        var listOfPatients = new ArrayList<Patient>();
        records.forEach((patientString) -> {
            listOfPatients.add(parse(patientString));
        });

        return listOfPatients;
    }
}
