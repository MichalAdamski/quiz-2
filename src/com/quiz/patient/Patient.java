package com.quiz.patient;

public class Patient {
    private final String name;
    private final int age;
    private final String sex;
    private final int hba1c;
    private final double insulin;
    private String diabetesType;

    public Patient(String name, int age, String sex, int hba1c, double insulin) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.hba1c = hba1c;
        this.insulin = insulin;
    }

    @Override
    public String toString() {
        var stringBuilder = new StringBuilder("Name: ").append(name).append("\n")
                .append("Age: ").append(age).append("\n")
                .append("Sex: ").append(sex).append("\n")
                .append("HbA1c: ").append(hba1c).append("\n")
                .append("Insulin: ").append(insulin).append("\n")
                .append("Diabetes type: ").append(diabetesType);

        return stringBuilder.toString();
    }

    public void setDiabetesType(String diabetesType) {
        if (diabetesType.equals("Diabetes Type 1") || diabetesType.equals("Diabetes Type 2") || diabetesType.equals("Normal (healthy)")){
            this.diabetesType = diabetesType;
        }
    }

    public String getName() {
        return name;
    }

    public int getHba1c() {
        return hba1c;
    }

    public double getInsulin() {
        return insulin;
    }
}
