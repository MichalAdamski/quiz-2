package com.quiz.classifier;

import com.quiz.patient.Patient;

public class DiabetesClassifier implements IDiabetesClassifier {

    public static final int INSULIN_NORMAL_THRESHOLD = 7;
    public static final int HBA1C_NORMAL_THRESHOLD_LOW_INSULIN = 48;
    public static final int HBA1C_NORMAL_THRESHOLD_HIGH_INSULIN = 42;

    public void classify(Patient patient){
        if(patient.getInsulin() < INSULIN_NORMAL_THRESHOLD && patient.getHba1c() > HBA1C_NORMAL_THRESHOLD_LOW_INSULIN) {
            patient.setDiabetesType("Diabetes Type 1");
        } else if(patient.getInsulin() >= INSULIN_NORMAL_THRESHOLD && patient.getHba1c() >= HBA1C_NORMAL_THRESHOLD_HIGH_INSULIN){
            patient.setDiabetesType("Diabetes Type 2");
        } else {
            patient.setDiabetesType("Normal (healthy)");
        }
    }
}
