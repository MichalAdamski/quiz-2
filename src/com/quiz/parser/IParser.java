package com.quiz.parser;

import java.util.List;

public interface IParser<T> {
    T parse(String line);
    List<T> parse(List<String> records);
}
