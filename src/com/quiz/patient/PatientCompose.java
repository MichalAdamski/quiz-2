package com.quiz.patient;

import com.quiz.classifier.DiabetesClassifier;
import com.quiz.classifier.IDiabetesClassifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PatientCompose {
    private final List<Patient> patients;
    private IDiabetesClassifier classifier;

    public PatientCompose(List<Patient> patients, IDiabetesClassifier classifier) {
        this.patients = patients;
        this.classifier = classifier;
        this.patients.forEach((patient) -> classifier.classify(patient));
    }

    public PatientCompose(IDiabetesClassifier classifier) {
        patients = new ArrayList<>();
        this.classifier = classifier;
    }

    public void addPatient(Patient patient) {
        patients.add(patient);
        classifier.classify(patient);
    }

    public void removePatient(Patient patient) {
        patients.remove(patient);
    }

    @Override
    public String toString() {
        var builder = new StringBuilder();

        patients.forEach((patient) -> {
            builder.append(patient.toString());
            builder.append("\n");
            builder.append("\n");
        });

        return builder.toString();
    }

    public void orderAlphabetically(){
        Collections.sort(patients, Comparator.comparing(Patient::getName).thenComparing(Patient::getInsulin));
    }

    public List<Patient> getAllCloseToNormalLevel(int margin){
        var insulinMargin = DiabetesClassifier.INSULIN_NORMAL_THRESHOLD * margin / 100.0;
        var insulinUpperMargin = DiabetesClassifier.INSULIN_NORMAL_THRESHOLD + insulinMargin;
        var insulinLowerMargin = DiabetesClassifier.INSULIN_NORMAL_THRESHOLD - insulinMargin;

        return patients.stream()
                .filter(patient -> patient.getInsulin() <  insulinUpperMargin && patient.getInsulin() > insulinLowerMargin)
                .collect(Collectors.toList());
    }
}
