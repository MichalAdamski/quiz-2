import com.quiz.classifier.DiabetesClassifier;
import com.quiz.loader.TextFileLoader;
import com.quiz.parser.PatientParser;
import com.quiz.patient.PatientCompose;

public class Main {
    public static void main(String[] args) {
        var list = TextFileLoader.loadText("patients.txt");

        if (list != null) {
            var parser = new PatientParser();
            var compose = new PatientCompose(parser.parse(list), new DiabetesClassifier());
            compose.orderAlphabetically();

            var filtered = compose.getAllCloseToNormalLevel(10);

            filtered.forEach(patient -> {
                System.out.println(patient);
                System.out.println("\n");
            });
        }
    }
}